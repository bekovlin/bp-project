import pandas as pd
import numpy as np

def create_data(file):
    f = open(f"data/initial_data/s100_{file}th.fasta", "r")
    new = open(f"data/data{file}.csv", "w")
    new.write('seq,occurrence,')
    for x in range(40):
        new.write(str(x))
        if( x == 39 ):
            new.write('\n')
        else:
            new.write(',')
    for x in f:
        if( x[0] == '>' ):
            end = x.find('-')
            res = x[1:end]
            new.write(res)
            new.write(',')
            end2 = x.find('\n')
            res = x[end+1:end2]
            new.write(res)
            new.write(',')
        elif( x != '\n' ):
            for i in range(len(x)):
                new.write(x[i])
                if(i != len(x) - 1 and i != len(x) - 2):
                    new.write(',')
            #new.write('\n')
    new.close()

def create_initial(file):
    f = open(f"data/initial_data/s100_{file}th.fasta", "r")
    new = open(f"data/initial{file}.csv", "w")
    new.write('num,occurrence,sequence\n')
    for x in f:
        if (x[0] == '>'):
            end = x.find('-')
            res = x[1:end]
            new.write(res)
            new.write(',')
            end2 = x.find('\n')
            res = x[end + 1:end2]
            new.write(res)
            new.write(',')
        elif (x != '\n'):
            new.write(x)
            # new.write('\n')
    new.close()


def matr():
    new = open("temp.txt", "w")
    for x in range(4):
        for i in range(40):
            new.write('0')
            if i == 39:
                new.write(' \\\\')
            else:
                new.write(" & ")
        new.write('\n')


def test_data_prep(og, nf):
    new = open(f"data/{nf}.csv", "w")
    old = open(f"data/{og}.csv", "r")

    new.write('occurrence,')
    for x in range(40):
        new.write(str(x))
        if (x == 39):
            new.write('\n')
        else:
            new.write(',')

    for x in old:
        sequences = x.split(',')
        new.write(sequences[1][0])
        new.write(',')
        for s in range(0,len(sequences[0])):
            new.write(sequences[0][s])
            if s == 39:
                new.write('\n')
            else:
                new.write(',')

    new.close()

def binders_and_non():
    f1 = open(f"data/binders.txt", "r")
    new = open(f"data/newly_generated_test.csv", "w")
    f2 = open(f"data/non-binders.txt", "r")

    lines1 = f1.readlines()
    lines2 = f2.readlines()

    for x in lines2:
        k = x.split('\n')[0]
        if k.startswith("\ufeff"):
            k = k.split("\ufeff")[1]
        print(k)
        for y in lines2:
            l = y.split('\n')[0]
            if l.startswith("\ufeff"):
                l = l.split("\ufeff")[1]
            if (k == l):
                continue
            print(k)
            new.write(k)
            new.write(l)
            new.write(',')
            new.write('0')
            new.write('\n')
        new.write(k)
        new.write(k)
        new.write(',')
        new.write('0')
        new.write('\n')

    for x in lines1:
        k = x.split('\n')[0]
        if k.startswith("\ufeff"):
            k = k.split("\ufeff")[1]
        print(k)
        for y in lines1:
            l = y.split('\n')[0]
            if l.startswith("\ufeff"):
                l = l.split("\ufeff")[1]
            if(k == l):
                continue
            new.write(k)
            new.write(l)
            new.write(',')
            new.write('1')
            new.write('\n')
        new.write(k)
        new.write(k)
        new.write(',')
        new.write('1')
        new.write('\n')
    new.close()

if __name__ == '__main__':
    binders_and_non()
    test_data_prep("newly_generated_test", "new_test_data")

    """
        for i in range(5,9):
        create_data(i)

    for i in range(5,9):
        create_initial(i)
    """

    #matr()
