import pandas as pd
import numpy as np

def preprocess(d1, d2, rep):
    data1 = pd.read_csv(f'../data/data{d1}.csv')
    data2 = pd.read_csv(f'../data/data{d2}.csv')

    joined = data1.merge(data2, on=['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39'], how='outer', indicator=True)

    joined.drop(["seq_x", 'seq_y', "occurrence_x"], axis=1, inplace=True)

    joined.loc[np.isnan(joined["occurrence_y"]), "occurrence_y"] = 0
    joined.occurrence_y.astype(int)

    if(rep):
        result = joined.loc[joined.index.repeat(joined.occurrence_y)]
        result.reset_index(inplace=True)
        result.drop(["index", "_merge"], axis=1, inplace=True)
        zeros = joined[joined['occurrence_y'] == 0]
        zeros.drop(["_merge"], axis=1, inplace=True)
        joined = pd.concat([result, zeros])

    joined['occurrence_y'] = np.where(joined['occurrence_y'] > 0, 1, 0)

    return joined


def create_matrix(joined):
    joined_matrix = [[[0] * 40 for i in range(4)] for n in range(len(joined))]

    joined.reset_index(inplace=True)

    for index, row in joined.iterrows():
        x = 0
        for i in row:
            if (i == 'A'):
                joined_matrix[index][0][x] = 1
            elif (i == 'C'):
                joined_matrix[index][1][x] = 1
            elif (i == 'G'):
                joined_matrix[index][2][x] = 1
            elif (i == 'T'):
                joined_matrix[index][3][x] = 1
            else:
                continue
            x += 1

    return joined_matrix